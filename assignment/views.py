from django.shortcuts import render
from datetime import datetime, date
import requests
import json

def index(request):
    return render(request, 'home.html')

def get_api(request):
    query = request.POST['query']
    if (query == ""):
        return render(request, 'home.html')

    resp = requests.get("https://www.metaweather.com/api/location/search/?query=" + query)
    content = json.loads(resp.text)

    temporary_content = []

    for item in content:
        it = "City: {}\nType: {}\nWoeid: {}\nLatt Long: {}\n".format(item['title'], item['location_type'], item['woeid'],
                                                                    item['latt_long'])
        temporary_content.append(it)

    response = {"content": temporary_content}
    return render(request, 'home.html', response)

