from django.conf.urls import url
from .views import index
from assignment import views

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^get_api$', views.get_api, name='get_api'),
]
